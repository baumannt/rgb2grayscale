'''
Created on 24.05.2018

@author: baumannt
'''

import sys
import os

from PIL import Image
import numpy as np

class RGB2Gray:
    
    in_file = ""
    out_file = ""
    
    def average(self):
        
        in_image = Image.open(self.in_file);
        
        rgb = np.array(in_image)
        gray = rgb
        
        for x in range(0, rgb.shape[0]):
            for y in range(0, rgb.shape[1]):
                
                tmp_sum = 0
                
                for z in range(0, rgb.shape[2]):
                    tmp_sum = tmp_sum + rgb[x][y][z]
                
                tmp_sum = tmp_sum / 3
                    
                gray[x][y][0] = tmp_sum
                gray[x][y][1] = tmp_sum
                gray[x][y][2] = tmp_sum
        
        out_image = Image.fromarray(gray)
        out_image.save(self.out_file)

    def luma(self, coeffs):
        
        in_image = Image.open(self.in_file);
        
        rgb = np.array(in_image)
        gray = rgb
        
        for x in range(0, rgb.shape[0]):
            for y in range(0, rgb.shape[1]):
                
                tmp_sum = 0
                
                for z in range(0, rgb.shape[2]):
                    tmp_sum = tmp_sum + coeffs[z]*rgb[x][y][z]
                    
                gray[x][y][0] = tmp_sum
                gray[x][y][1] = tmp_sum
                gray[x][y][2] = tmp_sum
        
        out_image = Image.fromarray(gray)
        out_image.save(self.out_file)

    def desaturation(self):
        
        in_image = Image.open(self.in_file);
        
        rgb = np.array(in_image)
        gray = rgb
        
        for x in range(0, rgb.shape[0]):
            for y in range(0, rgb.shape[1]):
                
                tmp_sum = int(max(rgb[x][y])) + int(min(rgb[x][y]))
                tmp_sum = tmp_sum / 2
                    
                gray[x][y][0] = tmp_sum
                gray[x][y][1] = tmp_sum
                gray[x][y][2] = tmp_sum
        
        out_image = Image.fromarray(gray)
        out_image.save(self.out_file)

if __name__ == '__main__':
    
    print("Let's convert a colored Bitmap image using different techniques.")
    converter = RGB2Gray()
    
    if len(sys.argv) == 1:
        in_file = "example/example.jpg"
        out_path = "example/output"
    else:
        in_file = sys.argv[1]
        out_path = sys.argv[2]
    
    converter.in_file = in_file
    
    if not os.path.isdir(out_path):
        os.makedirs(out_path)
    
    print("Averaging")
    converter.out_file = out_path + "/average.bmp"
    converter.average()
    
    print("Default Luma")
    converter.out_file = out_path + "/luma_default.bmp"
    converter.luma([0.3, 0.59, 0.11])
    
    print("ITU-R BT.709")
    converter.out_file = out_path + "/luma_BT709_large.bmp"
    converter.luma([0.2126, 0.7152, 0.0722])
    
    print("ITU-R BT.601")
    converter.out_file = out_path + "/luma_BT601_large.bmp"
    converter.luma([0.299, 0.587, 0.114])
    
    print("Desaturation")
    converter.out_file = out_path + "/desaturation_large.bmp"
    converter.desaturation()
    
    print("I'm done.")
    