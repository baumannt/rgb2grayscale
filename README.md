# RGB to Grayscale Converter

This Python application converts a colored bitmap image into a grayscale image.

## Usage

Run application and specify the input image and an output path parameters, e.g.

```
python rgb2grayscale path/to/image.bmp path/to/output_folder
```

If no paramters are specified, the example image `example/example.jpg` is used, and output will be created into `example/output`.

## Implemented Algorithms

### Averaging

```math
\text{Grey} = \frac{R + G + B}{3}
```

### Weighted Averaging

#### Default

```math
\text{Grey} = 0.3 \cdot R + 0.59 \cdot G + 0.11 \cdot B
```

#### ITU-R BT.709

```math
\text{Grey} = 0.2126 \cdot R + 0.7152 \cdot G + 0.0722 \cdot B
```

#### ITU-R BT.601

```math
\text{Grey} = 0.299 \cdot R + 0.587 \cdot G + 0.114 \cdot B
```

### Desaturation

```math
\text{Grey} = \frac{\text{max}(R, G, B) + \text{min}(R, G, B)}{2}
```